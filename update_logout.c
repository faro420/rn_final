/*
    @author: Mir Farshid Baha
    purpose: RNP-WS2016
    version: v0.9
*/

#include "ucp.h"
#include "user_list.h"

void outbound_update_logout(User* user, int socket){
    int n;
    uint32_t integer;
    uint32_t checksum = 5*sizeof(uint32_t) + strlen(user->name)+1;
    //sending version
    integer = VERSION;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending checksum   
    integer = checksum;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending type
    integer = UPDATE_LOG_IO;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending sub_type
    integer = LOGOUT;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending IP
    integer = user->ip;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending port
    integer = user->port;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending length
    integer = strlen(user->name)+1;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending username
    n = write(socket,user->name,strlen(user->name)+1);
}

void inbound_update_logout(int socket,char* buffer,uint32_t checksum){
    int n,i;
    uint32_t integer;
    User temp;
    temp.counter = 0;
    bzero(&temp, sizeof(User));;
    //reading user ip
    n = read (socket, &integer, sizeof(integer));
    integer = ntohl(integer);
    temp.ip = integer;
    //reading user port
    n = read (socket, &integer, sizeof(integer));
    integer = ntohl(integer);
    temp.port = integer;
    //reading user name length
    n = read (socket, &integer, sizeof(integer));
    integer = ntohl(integer);
    //reading the name
    n = read (socket, buffer, integer);
    buffer[integer-1]='\0';
    strcpy(temp.name,buffer);
    n = contains_user(&temp);
    if(n!=-1){
        for(i=0;i<list_size;i++){
           if(users[i].socket!=-1 && users[i].socket!=socket){
                outbound_update_logout(&temp,users[i].socket);
            }
        }
        remove_user(&users[n]);
    }
    print_user_list();
}
