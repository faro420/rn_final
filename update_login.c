/*
    @author: Mir Farshid Baha
    purpose: RNP-WS2016
    version: v0.9
*/

#include "ucp.h"
#include "user_list.h"

void outbound_update_login(User* user, int socket){
    int n,i;
    uint32_t integer;
    uint32_t checksum = 5*sizeof(uint32_t) + strlen(user->name)+1;
    //sending version
    integer = VERSION;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    if(n<0)remove_from_fdset(socket);
    //sending checksum   
    integer = checksum;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    if(n<0)remove_from_fdset(socket);
    //sending type
    integer = UPDATE_LOG_IO;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    if(n<0)remove_from_fdset(socket);
    //sending sub_type
    integer = LOGIN;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    if(n<0)remove_from_fdset(socket);
    //sending IP
    integer = user->ip;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    if(n<0)remove_from_fdset(socket);
    //sending port
    integer = user->port;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    if(n<0)remove_from_fdset(socket);
    //sending length
    integer = strlen(user->name)+1;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    if(n<0)remove_from_fdset(socket);
    //sending username
    n = write(socket,user->name,strlen(user->name)+1);
    if(n<0)remove_from_fdset(socket);
}

void inbound_update_login(int socket,char* buffer,uint32_t checksum){
    int n,i;
    uint32_t integer;
    User temp;
    bzero(buffer,400);
    bzero(&temp, sizeof(User));
    temp.counter = 0;
    temp.socket = -1;
    //reading user ip
    n = read (socket, &integer, sizeof(integer));
    if(n<0)remove_from_fdset(socket);
    integer = ntohl(integer);
    temp.ip = integer;
    //reading user port
    n = read (socket, &integer, sizeof(integer));
    if(n<0)remove_from_fdset(socket);
    integer = ntohl(integer);
    temp.port = integer;
    //reading user name length
    n = read (socket, &integer, sizeof(integer));
    if(n<0)remove_from_fdset(socket);
    integer = ntohl(integer);
    //reading the name
    n = read (socket,temp.name, integer);
    if(n<0)remove_from_fdset(socket);
    temp.name[integer-1]='\0';
    n = contains_user(&temp);
    if(n==-1){
        printf("%s just logged in!\n",temp.name);
        fflush(stdout);        
        add_user(&temp);
        for(i=0;i<list_size;i++){
           if(users[i].socket!=-1 && users[i].socket!=socket){
                outbound_update_login(&temp,users[i].socket);
            }
        }
    }
    print_user_list();
}
