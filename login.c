 /*
    @author: Mir Farshid Baha
    purpose: RNP-WS2016
    version: v0.9
*/

#include "ucp.h"
#include "user_list.h"

User currentUser;


void outbound_login(int socket, char* buffer){
    int n;
    bzero(&currentUser, sizeof(User));
    struct sockaddr_in ownAddress;
    getOwnData(socket, &ownAddress);
    //initialising currentUser
    //name
    strcpy(currentUser.name,buffer);
    //IP
    currentUser.ip = ntohl(ownAddress.sin_addr.s_addr);
    //port
    currentUser.port = ntohs(ownAddress.sin_port) ;
    //counter
    currentUser.counter= 0;
    //socket
    currentUser.socket = socket;
    //adding user to known user_list
    add_user(&currentUser);
    //writing a message
    printf("%s just logged in!\n", currentUser.name);
    fflush(stdout);
    //sending the actual login
    uint32_t checksum = 2*sizeof(uint32_t) + strlen(buffer)+1;
    //sending version
    uint32_t integer = VERSION;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    if(n<0)remove_from_fdset(socket);
    //sending checksum   
    integer = htonl(checksum);
    n = write(socket,&integer,sizeof(integer));
    if(n<0)remove_from_fdset(socket);
    //sending type
    integer = LOGIN;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    if(n<0)remove_from_fdset(socket);
    //sending length
    integer = strlen(buffer)+1;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    if(n<0)remove_from_fdset(socket);
    //sending username
    n = write(socket,buffer,strlen(buffer)+1);
    if(n<0)remove_from_fdset(socket);
    print_user_list();
}

void inbound_login(int socket, uint32_t checksum){
    struct sockaddr_in client_add;
    socklen_t size;
    size = sizeof(struct sockaddr_in);
    int n, nbytes,i;
    uint32_t length;
    User temp;
    bzero(&temp, sizeof(User));
    getpeername(socket, (struct sockaddr *)&client_add, &size);
    //reading username length
    nbytes = read (socket, &length, sizeof(length));
    if(nbytes<0)remove_from_fdset(socket);
    length = ntohl(length);
    //reading the name
    nbytes = read (socket, temp.name, length);
    if(nbytes<0)remove_from_fdset(socket);
    temp.name[length-1]='\0';
    temp.ip =ntohl(client_add.sin_addr.s_addr);
    temp.port= ntohs(client_add.sin_port);
    temp.socket = socket;
    temp.counter = 0;
    n = contains_user(&temp);
    if(n==-1){
        printf("%s just logged in!\n",temp.name);
        fflush(stdout);
        add_user(&temp);
        for(i=0;i<list_size;i++){
           if(users[i].socket!=-1 && users[i].socket!=socket){
                outbound_update_login(&temp,users[i].socket);
            }
        }
    }
    print_user_list();
}
