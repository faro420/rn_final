 /*
    @author: Mir Farshid Baha
    purpose: RNP-WS2016
    version: v0.9
*/
#include "ucp.h"


//declaration of synchronization elements
volatile int running;
int err;

int main(int argc, char *argv[]){
    Address add;
    pthread_t server_thread;
    if (argc < 3) {
        fprintf(stderr,"usage %s server_IP server_port \n", argv[0]);
        exit(0);
    }
    running = TRUE;
    strcpy(add.IP, argv[1]);
    add.port = atoi(argv[2]);
    init();
    err=pthread_create(&server_thread,NULL,server,(void*)&add);
    assert(!err);

    input_handler();
    
    err=pthread_join(server_thread,NULL);
    assert(err);
    cleanup();
    printf("\nALL THREADS JOINED SUCCESSFULY\n");
    fflush(stdout);
    return 0;
}
