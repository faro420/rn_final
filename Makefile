#@author: Mir Farshid Baha
#contact: mirfarshid.baha@haw-hamburg.de
#about: Makefile for the ultimate chat protocol
CC = /usr/bin/gcc
CFLAGS = -pthread -g -Wall -I.
Files = ucp.c server.c utility_functions.c input_handler.c login.c update_login.c logout.c update_client_list.c

ucp:ucp.c
	$(CC) $(CFLAGS) -o ucp $(Files) input_utility_fctns.c user_list.c update_logout.c message.c
clean:
	rm -f ucp
