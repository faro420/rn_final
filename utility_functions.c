 /*
    @author: Mir Farshid Baha
    purpose: RNP-WS2016
    version: v0.9
*/
 
#include "ucp.h"
#include "user_list.h"

//=====CREATE=SERVER=SOCKET==============
//+++++++++++++++++++++++++++++++++++++++
int server_socket (const char* IP, uint16_t port){
  int sock;
  struct sockaddr_in name;

  /* Create the socket. */
  sock = socket (PF_INET, SOCK_STREAM, 0);
  if (sock < 0){
      perror ("socket");
      exit (EXIT_FAILURE);
    }

  /* Give the socket a name. */
  name.sin_family = AF_INET;
  name.sin_port = htons (port);
  name.sin_addr.s_addr = inet_addr(IP);
  if (bind (sock, (struct sockaddr *) &name, sizeof (name)) < 0)
    {
      perror ("bind");
      exit (EXIT_FAILURE);
    }
  return sock;
}  
//===================END=================

//===============INIT====================
//+++++++++++++++++++++++++++++++++++++++
void init(){
    init_list();
    err=pthread_mutex_init(&mutex, NULL);
    assert(!err);  
}
//============INIT=END===================

//===============CLEANUP=================
//+++++++++++++++++++++++++++++++++++++++
void cleanup(){
    err=pthread_mutex_destroy(&mutex);
    assert(!err);
}
//=========CLEANUP=END===================

//===============CLEANUP=================
//+++++++++++++++++++++++++++++++++++++++
void getOwnData(int socket, struct sockaddr_in* addr){
	socklen_t size = sizeof(*(addr));
	if(getsockname(socket, (struct sockaddr*)addr, &size)==-1){
		perror("error extracting own ip\n");
	}
}

//=========CLEANUP=END===================
