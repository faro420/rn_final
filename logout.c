 /*
    @author: Mir Farshid Baha
    purpose: RNP-WS2016
    version: v0.9
*/

#include "ucp.h"
#include "user_list.h"

void outbound_logout(int socket){
    int n;
    //local deletion of the user
    printf("%s just logged out!\n", currentUser.name);
    remove_user(&currentUser);
    bzero(&currentUser, sizeof(User));
    //updating update_list length
    //sending the actual logout request
    uint32_t checksum = sizeof(uint32_t);
    uint32_t integer = VERSION;
    integer = htonl(integer);
    //sending version
    n = write(socket,&integer,sizeof(integer));
    if (n < 0) printf("ERROR write to socket");
    //sending checksum   
    integer = htonl(checksum);
    n = write(socket,&integer,sizeof(integer));
    if (n < 0) printf("ERROR write to socket");
    //sending type
    integer = LOGOUT;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    if (n < 0) printf("ERROR write to socket");
}

void inbound_logout(int socket){
    struct sockaddr_in client_add;
    socklen_t size;
    size = sizeof(struct sockaddr_in);
    int n, i;
    User temp;
    bzero(&temp, sizeof(User));
    getpeername(socket, (struct sockaddr *)&client_add, &size);
    //reading the name
    temp.ip =client_add.sin_addr.s_addr;
    temp.port= client_add.sin_port;
    n = contains_user(&temp);
    if(n!=-1){
        printf("%s logged out\n",users[n].name);        
        fflush(stdout);
        for(i=0;i<list_size;i++){
           if(users[i].socket!=-1&& users[i].socket!=socket){
                outbound_update_logout(&temp,users[i].socket);
            }
        }
        remove_user(&temp);
    }
    print_user_list();
}

