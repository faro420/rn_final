 /*
    @author: Mir Farshid Baha
    purpose: RNP-WS2016
    version: v0.9
*/
 
#ifndef USERLIST_H

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#define USERLEN 100

void init_list(void);
int contains_user(User* user);
void add_user(User* user);
void remove_user(User* user);
void swap_user(int index);
void print_user_list();

extern User users[USERLEN];
extern size_t list_size;

#endif
 
