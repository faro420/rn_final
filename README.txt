 /*
  *    @author: Mir Farshid Baha
  *    purpose: RNP-WS2016
  *    version: v0.9
  */
  
  UCP Chat Protokoll für das Praktikum Rechnernetze
  
  
  Bei der Implementation bedeutet + implementiert, - noch zu implementieren.
  implementiert:
               +login
               +logout
               +login update
               +logout update
               +message
               +message forwarding
               +update list
               
Benutzeranleitung:
    1 In der Konsole: make ucp
    2. Starten mit: ./ucp eigeneIP eigenerPort
       IP wird als String angenommen, Port als Zahl
    3. Danach läuft der Server, mit: connect
       verbindet man sich mit einem Server.
    4. Anschließend: login
       Man wird nach dem Namen gefragt.
    5. Einfach Nachrichten eingeben und auf ENTER drücken.
    6. Zum verlassen: logout
    7. Will man die Verbindung mit dem Server kappen: disconnect
    8. Um das Programm zu verlassen, nach disconnect: quit
    
Falls immer noch unklar ist, wie die Eingabe funktioniert, dann einen Blick auf
inputhandler.c werfen.
    
Viel Spaß :)
