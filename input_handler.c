 /*
  *    @author: Mir Farshid Baha
  *    purpose: RNP-WS2016
  *    version: v0.9
  */
 
 #include "ucp.h"
 
 void input_handler(){
     int sockfd, n;
     int connected = FALSE;
     int logged_in = FALSE;
     struct sockaddr_in serv_addr;
     char buffer[400];
     while(running){
         bzero(buffer,400);
         fgets(buffer,399,stdin);
         buffer[strcspn(buffer, "\n")] = '\0';
         if(connected==FALSE){
             if(!strcmp("connect", buffer)){
                 sockfd = connect_to_server(&serv_addr, buffer);
                 add_to_fdset(sockfd);
                 connected = TRUE;
             }else if(!strcmp("quit", buffer)){
                 running = FALSE;
                 printf("In Hamburg sag man Tschuess!\n");
                 fflush(stdout);
             }else{
                 printf("please type connect or quit\n");
                 fflush(stdout);
             }
         }else{
             if(logged_in == FALSE){
                 if(!strcmp("disconnect", buffer)){
                     remove_from_fdset(sockfd);
                     bzero(&serv_addr,sizeof(struct sockaddr_in));
                     printf("disconnected\n");
                     fflush(stdout);
                     connected = FALSE;
                 }else if(!strcmp("login", buffer)){
                     printf("Enter your username: ");
                     bzero(buffer,400);
                     fgets(buffer,399,stdin);
                     buffer[strcspn(buffer, "\n")] = '\0';
                     outbound_login(sockfd,buffer);
                     outbound_update_list(sockfd);
                     logged_in = TRUE;
                 }else{
                     printf("please type login or disconnect\n");
                     fflush(stdout);                     
                 }                 
             }else{
                 if(!strcmp("logout", buffer)){
                     outbound_logout(sockfd);
                     logged_in = FALSE;
                 }else{
                     outbound_message(sockfd,buffer);
                 }                   
             }
             
         }
     }
 }
 
 
