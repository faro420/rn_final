 /*
    @author: Mir Farshid Baha
    purpose: RNP-WS2016
    version: v0.9
*/
 
#include "ucp.h"

//============CONNECT====================
//+++++++++++++++++++++++++++++++++++++++
int connect_to_server(struct sockaddr_in* serv_addr,char* buffer){
    int sockfd;
    printf("Enter IP:");
    bzero(buffer,400);
    fgets(buffer,399,stdin);
    sockfd = socket(AF_INET, SOCK_STREAM, 0); 
    if (sockfd < 0) 
        printf("failed opening the socket\n");
    fflush(stdout); 
    serv_addr->sin_family = AF_INET;
    serv_addr->sin_addr.s_addr = inet_addr(buffer);            
    fflush(stdout);   
    printf("Enter Port:");
    fflush(stdout); 
    bzero(buffer,400);
    fgets(buffer,399,stdin);
    serv_addr->sin_port = htons(atoi(buffer));
    if (connect(sockfd,(struct sockaddr *)serv_addr,sizeof((*serv_addr))) < 0){  
        printf("failed to connect\n");
        fflush(stdout);
    }else{    
        printf("connected successfuly\n");
        fflush(stdout);        
    }
    fflush(stdout);
    return sockfd;
}
//============CONNECT==END===============
