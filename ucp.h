 /*
    @author: Mir Farshid Baha
    purpose: RNP-WS2016
    version: v0.9
*/
 
#ifndef UCP_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <pthread.h>
#include <assert.h>
#include <string.h>
#include<arpa/inet.h> //inet_addr

#define TRUE 1
#define FALSE 0
#define VERSION 42

#define LOGIN 100
#define MESSAGE 101
#define UPDATE_LOG_IO 102
#define LOGOUT 103
#define UPDATE_LIST 104

typedef struct{
    char IP[20];
    int port;
}Address;

typedef struct{
    char name[20];
    uint32_t ip;
    uint32_t port;
    uint32_t counter;
    int socket;
}User;

void init(void);
void* server(void*args);
int server_socket (const char* IP, uint16_t port);
void input_handler(void);
int connect_to_server(struct sockaddr_in*, char* buffer);
void add_to_fdset(int fd);
void remove_from_fdset(int fd);
void cleanup(void);

void outbound_login(int socket, char* buffer);
void inbound_login(int socket, uint32_t checksum);

void outbound_logout(int socket);
void inbound_logout(int socket);

void outbound_update_login(User* user, int socket);
void inbound_update_login(int socket,char* buffer,uint32_t checksum);

void outbound_update_logout(User* user, int socket);
void inbound_update_logout(int socket,char* buffer,uint32_t checksum);

void getOwnData(int socket, struct sockaddr_in* addr);
void outbound_message(int socket, char* buffer);
void inbound_message(int socket, char* buffer, uint32_t checksum);
void outbound_message_forward(int socket, char* buffer,User* user);

void outbound_update_list(int socket);
void outbound_update_forward(int socket, User* user);
void inbound_update_list(int socket, int checksum);

//declaration of synchronization elements
extern pthread_mutex_t mutex;
extern pthread_cond_t empty, full;
volatile extern int running;
extern int err;
extern User currentUser;

//declaration buffer related and variables

extern uint32_t update_length;
extern fd_set active_fd_set;
#endif
