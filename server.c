 /*
  *    @author: Mir Farshid Baha
  *    purpose: RNP-WS2016
  *    version: v0.9
  */
 
 #include "ucp.h"
 #include "user_list.h"
 
 pthread_mutex_t mutex;
 fd_set active_fd_set;
 
 void* server(void*args){
     char buffer[400];
     bzero(buffer,400);
     Address* add = ((Address*)args);
     int nbytes;
     int i, server_sock;
     uint32_t integer, checksum;
     fd_set read_fd_set;
     struct sockaddr_in clientname;
     socklen_t size;
     server_sock = server_socket (add->IP, add->port);
     if(listen (server_sock, 1) < 0){
         perror ("listen");
         exit (EXIT_FAILURE);
     }
     
     /* Initialize the set of active sockets. */
     FD_ZERO (&active_fd_set);
     FD_SET (server_sock, &active_fd_set);
     printf("Server laeuft!\n");
     int new;
     size = sizeof (clientname);
     while (running){
         /* Block until input arrives on one or more active sockets. */
         read_fd_set = active_fd_set;
         if (select (FD_SETSIZE, &read_fd_set, NULL, NULL, NULL) < 0){
             perror ("select");
             exit (EXIT_FAILURE);
         }
         /* Service all the sockets with input pending. */
         for (i = 0; i < FD_SETSIZE; ++i){
             if (FD_ISSET (i, &read_fd_set)){
                 if (i == server_sock){
                     /* Connection request on original socket. */
                     new = accept (server_sock,(struct sockaddr *) &clientname,&size);
                     if (new < 0){
                         perror ("accept");
                         exit (EXIT_FAILURE);
                     }
                     add_to_fdset(new);
                 }else{
                     nbytes = read (i, &integer, sizeof(integer));
                     if(nbytes<0)remove_from_fdset(i);
                     integer = ntohl(integer);
                     if(integer == VERSION){
                         nbytes = read (i, &integer, sizeof(integer));
                         if(nbytes<0)remove_from_fdset(i);
                         integer = ntohl(integer);
                         checksum = integer;
                         nbytes = read (i, &integer, sizeof(integer));
                         if(nbytes<0)remove_from_fdset(i);
                         integer = ntohl(integer);
                         switch(integer){
                             case LOGIN: inbound_login(i,checksum);outbound_update_list(i);;break;
                             case MESSAGE: inbound_message(i,buffer,checksum);break;
                             case UPDATE_LOG_IO: 
                                 nbytes = read (i, &integer, sizeof(integer));
                                 integer = ntohl(integer);
                                 if(integer == LOGIN){
                                     inbound_update_login(i,buffer,checksum);
                                 }else{
                                     inbound_update_logout(i,buffer,checksum);
                                 }
                                 break;
                             case LOGOUT: inbound_logout(i);break;
                             case  UPDATE_LIST:inbound_update_list(i,checksum);break;
                             default: printf("Error\n");fflush(stdout);break;
                         }
                     }
                 }
             }
         }
         
    }
         printf("out of scope\n");
         pthread_exit(NULL);
 }

//============REMOVE_FD==================
//+++++++++++++++++++++++++++++++++++++++
void remove_from_fdset(int fd){
    pthread_mutex_lock(&mutex);
    FD_CLR (fd, &active_fd_set);
    close (fd);
    pthread_mutex_unlock(&mutex);    
}
//==========REMOVE_FD==END===============

//===============ADD_FD==================
//+++++++++++++++++++++++++++++++++++++++
void add_to_fdset(int fd){
    pthread_mutex_lock(&mutex);
    FD_SET (fd, &active_fd_set);
    pthread_mutex_unlock(&mutex);
}
//=============ADD_FD==END===============
