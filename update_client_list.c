/*
    @author: Mir Farshid Baha
    purpose: RNP-WS2016
    version: v0.9
*/

#include "ucp.h"
#include "user_list.h"

void outbound_update_list(int socket){
    int n,i;
    //increasing counter
    currentUser.counter++;
    //assigning total length
    uint32_t checksum = update_length+4*sizeof(uint32_t);
    //sending version
    uint32_t integer = VERSION;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending checksum   
    integer = htonl(checksum);
    n = write(socket,&integer,sizeof(integer));
    //sending type
    integer = UPDATE_LIST;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending IP Origin
    integer = currentUser.ip;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending Port Origin
    integer = currentUser.port;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending counter
    integer = currentUser.counter;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    
    for(i=0;i<list_size;i++){
        //sending IP Origin
        integer = users[i].ip;
        integer = htonl(integer);
        n = write(socket,&integer,sizeof(integer)); 
        //sending Port Origin
        integer = users[i].port;
        integer = htonl(integer);
        n = write(socket,&integer,sizeof(integer));
        //sending name length
        integer = strlen(users[i].name)+1;
        integer = htonl(integer);
        n = write(socket,&integer,sizeof(integer));
        //sending name 
        n = write(socket,users[i].name,strlen(users[i].name)+1);
    }    
}

void inbound_update_list(int socket, int checksum){
    int n, i;
    uint32_t integer, length, total_length;
    total_length = checksum - 4*sizeof(uint32_t);
    User temp, temp_foward;
    bzero(&temp, sizeof(User));    
    //getting IP Origin
    n = read(socket,&integer,sizeof(integer));
    integer = ntohl(integer);
    temp.ip = integer;
    temp_foward.ip = integer;
    //getting Port Origin
    n = read(socket,&integer,sizeof(integer));
    integer = ntohl(integer);
    temp.port = integer;
    temp_foward.port = integer;
    //getting counter
    n = read(socket,&integer,sizeof(integer));
    integer = ntohl(integer);
    temp.counter = integer;
    temp_foward.ip = integer;
    if(total_length > 0){
        n = contains_user(&temp);
        if(n!=-1){
            if(users[n].counter < temp.counter){
                users[n].counter = temp.counter;
                while(total_length >0){
                    bzero(&temp, sizeof(User));
                    temp.counter = 0;
                    temp.socket = -1;
                    //getting IP
                    n = read(socket,&integer,sizeof(integer));
                    integer = ntohl(integer);
                    temp.ip = integer;
                    //getting Port Origin
                    n = read(socket,&integer,sizeof(integer));
                    integer = ntohl(integer);
                    temp.port = integer;
                    //getting length of the name
                    n = read(socket,&integer,sizeof(integer));
                    integer = ntohl(integer);
                    total_length = total_length - integer - 3*sizeof(uint32_t);
                    //reading the name
                    n = read(socket,temp.name,integer);
                    n = contains_user(&temp);
                    if(n==-1){
                        add_user(&temp);
                    }
                }
                for(i=0;i<list_size;i++){
                    if(users[i].socket!=-1){
                        outbound_update_forward(users[i].socket,&temp_foward);
                    }
                }
            }
        }
    }
}

void outbound_update_forward(int socket, User* user){
    int n,i;
    //assigning total length
    uint32_t checksum = update_length+4*sizeof(uint32_t);
    //sending version
    uint32_t integer = VERSION;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending checksum   
    integer = htonl(checksum);
    n = write(socket,&integer,sizeof(integer));
    //sending type
    integer = UPDATE_LIST;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending IP Origin
    integer = user->ip;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending Port Origin
    integer = user->port;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending counter
    integer = user->counter;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    
    for(i=0;i<list_size;i++){
        //sending IP Origin
        integer = users[i].ip;
        integer = htonl(integer);
        n = write(socket,&integer,sizeof(integer)); 
        //sending Port Origin
        integer = users[i].port;
        integer = htonl(integer);
        n = write(socket,&integer,sizeof(integer));
        //sending name length
        integer = strlen(users[i].name)+1;
        integer = htonl(integer);
        n = write(socket,&integer,sizeof(integer));
        //sending name 
        n = write(socket,users[i].name,strlen(users[i].name)+1);
    }    
}
