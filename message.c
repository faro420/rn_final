/*
    @author: Mir Farshid Baha
    purpose: RNP-WS2016
    version: v0.9
*/

#include "ucp.h"
#include "user_list.h"

void outbound_message(int socket, char* buffer){
    int n,i;
    //incrementing msg counter
    currentUser.counter++;
    uint32_t checksum = 5*sizeof(uint32_t) + strlen(buffer)+1;
    //sending version
    uint32_t integer = VERSION;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending checksum   
    integer = htonl(checksum);
    n = write(socket,&integer,sizeof(integer));
    //sending type
    integer = MESSAGE;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending IP Origin
    integer = currentUser.ip;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending Port Origin
    integer = currentUser.port;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending counter
    integer = currentUser.counter;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending length
    integer = strlen(buffer)+1;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending the actual f****** message
    n = write(socket,buffer,strlen(buffer)+1);
}

void inbound_message(int socket, char* buffer, uint32_t checksum){
    int n, i;
    uint32_t integer, length;
    User temp;
    bzero(&temp, sizeof(User));
    bzero(buffer, 400);    
    //getting IP Origin
    n = read(socket,&integer,sizeof(integer));
    integer = ntohl(integer);
    temp.ip = integer;
    //getting Port Origin
    n = read(socket,&integer,sizeof(integer));
    integer = ntohl(integer);
    temp.port = integer;
    //getting counter
    n = read(socket,&integer,sizeof(integer));
    integer = ntohl(integer);
    temp.counter = integer;
    //getting length of the message
    n = read(socket,&integer,sizeof(integer));
    integer = ntohl(integer);
    length = integer;
    //getting the actual f****** message
    n = read(socket,buffer,length);
    n = contains_user(&temp);
    if(n!=-1){
        if(users[n].counter < temp.counter){
            printf("%s:%s\n", users[n].name,buffer);
            fflush(stdout);
            for(i=0;i<list_size;i++){
                if(users[i].socket!=-1){
                    outbound_message_forward(users[i].socket, buffer,&temp);
                }
            }         
        }
        users[n].counter = temp.counter;
    }
}

void outbound_message_forward(int socket, char* buffer,User* user){
    int n,i;
    uint32_t checksum = 5*sizeof(uint32_t) + strlen(buffer)+1;
    //sending version
    uint32_t integer = VERSION;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending checksum   
    integer = htonl(checksum);
    n = write(socket,&integer,sizeof(integer));
    //sending type
    integer = MESSAGE;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending IP Origin
    integer = user->ip;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending Port Origin
    integer = user->port;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending counter
    integer = user->counter;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending length
    integer = strlen(buffer)+1;
    integer = htonl(integer);
    n = write(socket,&integer,sizeof(integer));
    //sending the actual f****** message
    n = write(socket,buffer,strlen(buffer)+1);
}
