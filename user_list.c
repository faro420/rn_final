 /*
  *   @author: Mir Farshid Baha
  *   purpose: RNP-WS2016
  *   version: v0.9
  */
#include "ucp.h"
#include "user_list.h"
 
 User users[USERLEN];
 //application related variables
 uint32_t update_length;
 size_t list_size;
 
 void init_list(){
     int i;
     for(i=0;i<USERLEN;i++){
         bzero(users[i].name,20);
         users[i].ip = 0;
         users[i].port = 0;        
         users[i].counter = 0;
         users[i].socket = -1;
     }    
     list_size = update_length = 0;
 }
 
 int contains_user(User* user){
     int i;
     for(i=0;i<list_size;i++){
         if(users[i].ip == user->ip && users[i].port == user->port && strcmp(user->name,users[i].name ))
             return i;       
     }
     return -1;
 }
 void add_user(User* user){
     if(list_size < (USERLEN)+1){
         strcpy(users[list_size].name,user->name);
         users[list_size].ip = user->ip;
         users[list_size].port = user->port;        
         users[list_size].counter = user->counter;
         users[list_size].socket = user->socket;
         list_size++;
         //calculating length for list update_length
         update_length = update_length + (strlen(user->name)+1)+ 3*sizeof(uint32_t);
     }else{
         printf("The list is full\n");
         fflush(stdout);
     }
 }
 void remove_user(User* user){
     int i = contains_user(user);
     if(i!=-1){
         //calculating length for list update_length
         update_length = update_length - (strlen(user->name)+1)- 3*sizeof(uint32_t);
         swap_user(i);
         list_size--;
     }
 }
 
 void swap_user(int index){
     User temp;
     //deleting user[index]
     bzero(users[index].name,20);
     users[index].ip = 0;
     users[index].port = 0;        
     users[index].counter = 0;
     users[index].socket = -1;
     // storing users[list_size-1] in temp
     strcpy(temp.name,users[list_size-1].name);
     temp.ip = users[list_size-1].ip;
     temp.port = users[list_size-1].port;        
     temp.counter = users[list_size-1].counter;
     temp.socket = users[list_size-1].socket;
     //deleting users[list_size-1]
     bzero(users[list_size-1].name,20);
     users[list_size-1].ip = 0;
     users[list_size-1].port = 0;        
     users[list_size-1].counter = 0;
     users[list_size-1].socket = -1;
     //copying temp to user[index]
     strcpy(users[index].name, temp.name);
     users[index].ip = temp.ip;
     users[index].port = temp.port;        
     users[index].counter = temp.counter;
     users[index].socket = temp.socket;
 }
 
void print_user_list(){
     int i;
     struct in_addr ip_addr;
     printf("\n******************\n");
     fflush(stdout);
     for(i=0;i<list_size;i++){
	ip_addr.s_addr = ntohl(users[i].ip);
        printf("[username:%s|user_ip:%s|user_port:%u|user_counter:%u]\n",
            users[i].name,inet_ntoa(ip_addr),users[i].port,users[i].counter);
     }
     fflush(stdout);
     printf("\n******************\n");
     fflush(stdout);
}
